#help resource
#http://nbviewer.ipython.org/github/ptwobrussell/Mining-the-Social-Web-2nd-Edition/blob/master/ipynb/Chapter%202%20-%20Mining%20Facebook.ipynb

# Copy and paste in the value you just got from the inline frame into this variable and execute this cell.
# Keep in mind that you could have just gone to https://developers.facebook.com/tools/access_token/
# and retrieved the "User Token" value from the Access Token Tool

ACCESS_TOKEN = ''
import facebook # pip install facebook-sdk
import json
from pprint import pprint
import logging
#import db_handling as db_h
import os,sys,inspect,glob
import time
from subprocess import call
import random
from datetime import datetime
import csv,re

#setting path for parent directory
parentdir = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
sys.path.insert(0,parentdir)

class CrawlFacebook:

    # A helper function to pretty-print Python objects as JSON
    def pp(o):
        print json.dumps(o, indent=1)

    #function to rename txt to csv
    def rename(dir, pattern, titlePattern):
        for pathAndFilename in glob.iglob(os.path.join(dir, pattern)):
            title, ext = os.path.splitext(os.path.basename(pathAndFilename))
            os.rename(pathAndFilename,
                      #os.path.join(dir, titlePattern % title + ext))
                      os.path.join(dir, titlePattern % title))

    # Create a connection to the Graph API with your access token
    g = facebook.GraphAPI(ACCESS_TOKEN)

    '''
    # Execute a few sample queries

    print '---------------'
    print 'Me'
    print '---------------'
    #pp(g.get_object('me'))
    print
    print '---------------'
    print 'My Friends'
    print '---------------'
    #pp(g.get_connections('me', 'friends'))
    print
    print '---------------'
    print 'Social Web'
    print '---------------'
    #pp(g.request("search", {'q' : 'social web', 'type' : 'page'}))
    '''

    fileFbIds= parentdir+'/crawl_facebook/fbPageIds.csv'
    count=0
    # Get an instance of Mining the Social Web
    # Using the page name also works if you know it.
    # e.g. 'MiningTheSocialWeb' or 'CrossFit'
    #page_id
    #reading facebook ids
    with open('%s'%(fileFbIds), mode='r') as infile1:
        reader1 = csv.reader(infile1)
        for row1 in reader1:
            temp1= row1[0]
            tempId= temp1.split(';')
            #print tempId[0]
            #mtsw_id = '67253243887'
            mtsw_id = str(tempId[0])
            count=count+1

            '''
            pageId/posts- https://developers.facebook.com/docs/graph-api/reference/page
            edges in page means using page_id what other info, like posts  you can get,
            other fields(without /) shows properties for page id like 'id','name','username'
            later, try for other than page id....
            '''

            #properties of page
            #pp(g.get_object(mtsw_id))
            obj=g.get_object(mtsw_id)
            print "username:"
            pprint(obj["username"])
            '''
            try:
                print "Page Id:"
                pprint(obj["id"])
                print "name:"
                pprint(obj["name"])
                print "username:"
                pprint(obj["username"])
                print "likes:"
                pprint(obj["likes"])
                print "talking_about_count:"
                pprint(obj["talking_about_count"])
                print "checkins:"
                pprint(obj["checkins"])
            except Exception:
                print Exception
            '''

            #setting folder path
            #folder_name3 = parentdir+'/crawl_facebook/graph_data/%s'%speaker[2]
            folder_name3 = parentdir+'/crawl_facebook/fb_reports/%s'%str(obj["username"])
            folderListFbUsers = parentdir+'/crawl_facebook/fb_users'

            if not os.path.exists(folder_name3):
                os.makedirs(folder_name3)

                if not os.path.exists(folderListFbUsers):
                    os.makedirs(folderListFbUsers)

                #writing page details in file
                with open('%s/%s.txt'%(folder_name3,str(obj["username"])+"_PageDetails"), 'w') as f2:
                    #f2.write("%s %s %s %s %s %s\n" % (str(obj["id"]),str(obj["name"]),str(obj["username"]),str(obj["likes"]),str(obj["talking_about_count"]),str(obj["checkins"])))
                    f2.write("%s %s %s %s %s \n" % (str(obj["id"]),str(obj["username"]),str(obj["likes"]),str(obj["talking_about_count"]),str(obj["checkins"])))

                with open('%s/%s.txt'%(folderListFbUsers,"listFbUsers"), 'a+') as f3:
                    #f3.write("%s %s %s %s %s %s %s\n" % (str(count),str(obj["id"]),str(obj["name"]),str(obj["username"]),str(obj["likes"]),str(obj["talking_about_count"]),str(obj["checkins"])))
                    f3.write("%s %s %s %s %s %s \n" % (str(count),str(obj["id"]),str(obj["username"]),str(obj["likes"]),str(obj["talking_about_count"]),str(obj["checkins"])))

                #details of posts on the page, in api as '/posts', '/likes'
                #pp(g.get_connections(mtsw_id, 'posts'))
                #pp(g.get_connections(mtsw_id, 'likes'))

                #graph.get_object('coldplay/feed', limit=2)
                #http://stackoverflow.com/questions/14029085/python-facebook-sdk-graph-api-access-error
                #getting most recent 100 posts
                data=g.get_connections(mtsw_id, 'posts', limit=100)
                '''
                limitEnd=100
                try:
                    print "inside 200"
                    data=g.get_connections(mtsw_id, 'posts', limit=200)
                    limitEnd=200
                except Exception:
                    print Exception
                    data=g.get_connections(mtsw_id, 'posts', limit=100)
                    limitEnd=100
                '''


                #pprint(data)
                #pprint(data["data"][1]["shares"])
                #pprint(data["data"][1]["created_time"])
                #pprint(data["data"][1]["link"])


                '''
                print "limitEnd-",limitEnd
                for i in range(1,limitEnd):
                '''
                for i in range(1,100):

                    #test print
                    #print "shares:"
                    #pprint(data["data"][i]["shares"]["count"])
                    #print "created_time:"
                    #pprint(data["data"][i]["created_time"])
                    #print "post_id:"
                    #pprint(data["data"][i]["id"])
                    #try:
                        #print "post_link:"
                        #pprint(data["data"][i]["link"])
                    #except NameError:
                        #print 'false'

                    str1=''
                    str2=''
                    str3=''
                    str4=''
                    #exception handling for null links
                    try:
                        str1=str(data["data"][i]["link"])
                    except Exception:
                        print Exception

                    try:
                        str3=str(data["data"][i]["shares"]["count"])
                    except Exception:
                        print Exception

                    try:
                        s=str(data["data"][i]["created_time"])
                    except Exception:
                        print Exception

                    try:
                        str4=str(data["data"][i]["message"])
                    except Exception:
                        print Exception

                    '''
                    #more than one post can be posted so timestamp with date and time important
                    tempTime=time.mktime(datetime.utctimetuple(datetime.strptime(s, "%Y-%m-%dT%H:%M:%S+0000")))
                    timeUtc=int(tempTime)
                    '''

                    #'''
                    #converting to date wise removing time
                    tempTime=datetime.strptime(s, "%Y-%m-%dT%H:%M:%S+0000").date()
                    temp2Time=time.mktime(datetime.utctimetuple(datetime.strptime(str(tempTime), "%Y-%m-%d")))+3600
                    timeUtc=int(temp2Time)
                    #'''
                    print "str3=",str3
                    if not str3:
                        str3='0'
                        print "str3=",str3

                    print "str4=",str4
                    if not str4:
                        str4='NoDesc'
                        print "str4=",str4

                    print "str1=",str1
                    if not str1:
                        str1='NoLink'
                        print "str1=",str1

                    #'a+' using append in csv it does not write results in sequence of input
                    with open('%s/%s.txt'%(folder_name3,str(obj["username"])+"_postDetails"), 'a+') as f:
                        if str1:
                            #f.write("%s %s %s %s\n" % (str(data["data"][i]["created_time"]),str(data["data"][i]["id"]),str(data["data"][i]["shares"]["count"]),str1))
                            try:
                                f.write("%s %s %s %s\n" % (timeUtc,str(data["data"][i]["id"]),str3,str1))
                            except Exception:
                                print Exception
                        else:
                            #f.write("%s %s %s %s\n" % (str(data["data"][i]["created_time"]),str(data["data"][i]["id"]),str(data["data"][i]["shares"]["count"]),str2))
                            try:
                                f.write("%s %s %s %s\n" % (timeUtc,str(data["data"][i]["id"]),str3,str2))
                            except Exception:
                                print Exception

                    #'a+' using append in csv it does not write results in sequence of input
                    with open('%s/%s.txt'%(folder_name3,str(obj["username"])+"_postDetailsMessage"), 'a+') as f:
                        #logic to remove special characters from post
                        str4=str4.replace(" ","SPACE")
                        str4=str4.replace("\n","-")
                        str4=re.sub('[^A-Za-z0-9]+', '', str4)
                        str4=str4.replace("SPACE"," ")
                        if str1:
                            #f.write("%s %s %s %s\n" % (str(data["data"][i]["created_time"]),str(data["data"][i]["id"]),str(data["data"][i]["shares"]["count"]),str1))
                            try:
                                f.write("%s#+%s#+%s#+%s#+%s \n" % (timeUtc,str(data["data"][i]["id"]),str3,str4,str1))
                            except Exception:
                                print Exception
                        else:
                            #f.write("%s %s %s %s\n" % (str(data["data"][i]["created_time"]),str(data["data"][i]["id"]),str(data["data"][i]["shares"]["count"]),str2))
                            try:
                                f.write("%s#+%s#+%s#+%s#+%s \n" % (timeUtc,str(data["data"][i]["id"]),str3,str4,str2))
                            except Exception:
                                print Exception

                    #timestam and share count graph
                    with open('%s/%s.txt'%(folder_name3,str(obj["username"])), 'a+') as f4:
                        try:
                            f4.write("%s %s\n" % (timeUtc,str3))
                        except Exception:
                            print Exception

                #renaming txt to csv
                rename(r'%s'%folder_name3, r'*.txt', r'%s.csv')
        rename(r'%s'%folderListFbUsers, r'*.txt', r'%s.csv')