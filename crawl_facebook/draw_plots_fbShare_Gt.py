#!/usr/bin/env python

#import db_handling as hand
import os,sys,inspect,time
from subprocess import call

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
sys.path.insert(0,parentdir)
import db_handling as han




class DrawGraphsFbShareGt:

    def __init__(self,db_keys):

        self.handler = han.DBHandler(db_keys)
    """
    It takes all the files in the 'reports' folder and calls simple_octave script for each csv.
    """
    def plot_fbShareGt(self):                #make plots according to type given
        #print len(os.listdir('../reports'))
        speakers = self.handler.get_users_with_fullname_username()
        #SCRIPT= "/home/shukla/semester_project/twitter_project_eurecom/scrape_intraceder/simple_octave"
        for i,speaker in enumerate(speakers):
            folder_name4 = parentdir+'/crawl_facebook/plot_graph_fbShare_Gt/'
            if not os.path.exists(folder_name4):
                os.makedirs(folder_name4)
            #if ((speakers[4] != 'Debbie Harry/BLONDIE') or (speakers[4] != 'Dawn Richard / NEON') or (speakers[4]!='R. Dugdale/D. Petrie')):

            folder_name3 = parentdir+'/crawl_facebook/graph_data_fbShare_Gt/%s'%speaker[2]

            try:                                 #run octave script on the contents of csv taking 'x' as google trend volume and 'y' as timestamp

                print "SIMPLEE"
                print '%s/%s.csv'%(folder_name3,str(speaker[2]))

                #print os.listdir('./scrape_intraceder')
                #print parentdir
                #call(["octave","--silent", "--eval","simple_octave", "%s/%s.csv"%(folder_name3,str(speaker[2])),"%s/%s"%(folder_name4,str(speaker[2]))+".jpeg","%s"%(str(speaker[4])+"-"+str(speaker[3]))]) #calling bash commands for octave
                call(["octave","--silent", "--eval","simple_octave_fbShare_gt", "%s/%s.csv"%(folder_name3,str(speaker[2])),"%s/%s"%(folder_name4,str(speaker[2]))+".jpeg","%s"%(str(speaker[5])+"-"+str(speaker[3]))]) #calling bash commands for octave



            except Exception as e:
                print e





