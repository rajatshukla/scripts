__author__ = 'shukla'

import logging
import db_handling as db_h
import os,sys,inspect
import time
from datetime import datetime
import csv,glob,subprocess
from itertools import imap

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
sys.path.insert(0,parentdir)


class CalCorelatione:

#def __init__(self,twitter_keys,db_keys):
    def __init__(self,db_keys):
        self.token_list=[]
        #self.get_authentications(twitter_keys)
        self.handler = db_h.DBHandler(db_keys)

        self.logger = logging.getLogger('Scrape_InterCeder')
        fh=logging.FileHandler('interCeder_scrape.log')
        fh.setLevel(logging.DEBUG)
        self.logger.addHandler(fh)

    #function to rename txt to csv
    def rename(self,dir, pattern, titlePattern):
        for pathAndFilename in glob.iglob(os.path.join(dir, pattern)):
            title, ext = os.path.splitext(os.path.basename(pathAndFilename))
            os.rename(pathAndFilename,
                      #os.path.join(dir, titlePattern % title + ext))
                      os.path.join(dir, titlePattern % title))


    def getGraphDataMerge(self):

        speakers = self.handler.get_users_with_fullname_username()
        ts = int(time.time())

        for i,speaker in enumerate(speakers):

            try:
                #if ((speakers[4] != 'Debbie Harry/BLONDIE') or (speakers[4] != 'Dawn Richard / NEON') or (speakers[4]!='R. Dugdale/D. Petrie')):
                #folder_name = parentdir+'/scrape_intraceder/interceder_reports/%s'%speaker[2]
                #retweets vs interNews
                folder_name = parentdir+'/crawl_newsCred/allGraphData/interceder/graph_data/%s'%speaker[2]
                #retweets vs gt
                folder_name2 = parentdir+'/crawl_newsCred/allGraphData/gtRetweets/crawled_reports/'
                #fbLikes vs interNews
                folder_name3=parentdir+'/crawl_newsCred/allGraphData/interceder/graph_data_fbInterceder/%s'%speaker[2]
                #fbShare vs interNews
                folder_name4=parentdir+'/crawl_newsCred/allGraphData/interceder/graph_data_fbShare_Interceder/%s'%speaker[2]

                #sharecount
                folder_name5=parentdir+'/crawl_newsCred/allGraphData/fb/fb_reports/%s'%speaker[6]
                #gt vs fbshares
                folder_name6=parentdir+'/crawl_newsCred/allGraphData/fb/graph_data_fbShare_Gt/%s'%speaker[2]
                #retweets vs fbshare
                folder_name7=parentdir+'/crawl_newsCred/allGraphData/fb/graph_data_fbShare_Retweets/%s'%speaker[2]
                #likeCount
                folder_name8=parentdir+'/crawl_newsCred/allGraphData/fb/FinalLikeResults/%s'%speaker[6]

                #gt vs interNews
                folder_name9 = parentdir+'/crawl_newsCred/allGraphData/interceder/graph_data_interceder_gt/%s'%speaker[2]

                #twfollowers vs interNews
                folder_name10 = parentdir+'/crawl_newsCred/allGraphData/interceder/graph_data_interceder_twfollowers/%s'%speaker[2]

                #twfollowers vs fbLikes
                folder_name11 = parentdir+'/crawl_newsCred/allGraphData/fb/graph_data_fbLike_twfollowers/%s'%speaker[2]

                #twfollowers vs fbShare
                folder_name12 = parentdir+'/crawl_newsCred/allGraphData/fb/graph_data_fbShare_twfollowers/%s'%speaker[2]

                #gt vs fbLikes
                folder_name13 = parentdir+'/crawl_newsCred/allGraphData/fb/graph_data_fbLike_gt/%s'%speaker[2]

                #retweets vs fbLikes
                folder_name14 = parentdir+'/crawl_newsCred/allGraphData/fb/graph_data_fbLike_retweets/%s'%speaker[2]


                #output merged results
                #retweets vs interNews
                folder_name_out = parentdir+'/crawl_newsCred/allGraphData/interceder/corelation_graph_data/%s'%speaker[2]
                folder_name_out_p = parentdir+'/crawl_newsCred/allGraphData/interceder/corelation_graph_data'

                #retweets vs gt
                folder_name_out2 = parentdir+'/crawl_newsCred/allGraphData/gtRetweets/corelation_crawled_reports'
                folder_name_out2_p = parentdir+'/crawl_newsCred/allGraphData/gtRetweets/corelation_crawled_reports'

                #fbLikes vs interNews
                folder_name_out3=parentdir+'/crawl_newsCred/allGraphData/interceder/corelation_graph_data_fbInterceder/%s'%speaker[2]
                folder_name_out3_p=parentdir+'/crawl_newsCred/allGraphData/interceder/corelation_graph_data_fbInterceder'

                #fbShare vs interNews
                folder_name_out4=parentdir+'/crawl_newsCred/allGraphData/interceder/corelation_graph_data_fbShare_Interceder/%s'%speaker[2]
                folder_name_out4_p=parentdir+'/crawl_newsCred/allGraphData/interceder/corelation_graph_data_fbShare_Interceder'

                #sharecount
                folder_name_out5=parentdir+'/crawl_newsCred/allGraphData/fb/corelation_fb_reports/%s'%speaker[6]
                folder_name_out5_p=parentdir+'/crawl_newsCred/allGraphData/fb/corelation_fb_reports'

                #gt vs fbshares
                folder_name_out6=parentdir+'/crawl_newsCred/allGraphData/fb/corelation_graph_data_fbShare_Gt/%s'%speaker[2]
                folder_name_out6_p=parentdir+'/crawl_newsCred/allGraphData/fb/corelation_graph_data_fbShare_Gt'

                #retweets vs fbshare
                folder_name_out7=parentdir+'/crawl_newsCred/allGraphData/fb/corelation_graph_data_fbShare_Retweets/%s'%speaker[2]
                folder_name_out7_p=parentdir+'/crawl_newsCred/allGraphData/fb/corelation_graph_data_fbShare_Retweets'

                #likeCount
                folder_name_out8=parentdir+'/crawl_newsCred/allGraphData/fb/corelation_FinalLikeResults/%s'%speaker[6]
                folder_name_out8_p=parentdir+'/crawl_newsCred/allGraphData/fb/corelation_FinalLikeResults'

                #gt vs interNews
                folder_name_out9 = parentdir+'/crawl_newsCred/allGraphData/interceder/corelation_graph_data_interceder_gt/%s'%speaker[2]
                folder_name_out9_p = parentdir+'/crawl_newsCred/allGraphData/interceder/corelation_graph_data_interceder_gt'

                #twfollowers vs interNews
                folder_name_out10 = parentdir+'/crawl_newsCred/allGraphData/interceder/corelation_graph_data_interceder_twfollowers/%s'%speaker[2]
                folder_name_out10_p = parentdir+'/crawl_newsCred/allGraphData/interceder/corelation_graph_data_interceder_twfollowers'

                #twfollowers vs fbLikes
                folder_name_out11 = parentdir+'/crawl_newsCred/allGraphData/fb/corelation_graph_data_fbLikes_twfollowers/%s'%speaker[2]
                folder_name_out11_p = parentdir+'/crawl_newsCred/allGraphData/fb/corelation_graph_data_fbLikes_twfollowers'

                #twfollowers vs fbShare
                folder_name_out12 = parentdir+'/crawl_newsCred/allGraphData/fb/corelation_graph_data_fbShare_twfollowers/%s'%speaker[2]
                folder_name_out12_p = parentdir+'/crawl_newsCred/allGraphData/fb/corelation_graph_data_fbShare_twfollowers'

                #gt vs fbLikes
                folder_name_out13 = parentdir+'/crawl_newsCred/allGraphData/fb/corelation_graph_data_fbLikes_gt/%s'%speaker[2]
                folder_name_out13_p = parentdir+'/crawl_newsCred/allGraphData/fb/corelation_graph_data_fbLikes_gt'

                #retweets vs fbLikes
                folder_name_out14 = parentdir+'/crawl_newsCred/allGraphData/fb/corelation_graph_data_fbLikes_retweets/%s'%speaker[2]
                folder_name_out14_p = parentdir+'/crawl_newsCred/allGraphData/fb/corelation_graph_data_fbLikes_retweets'




                '''
                try:
                    with open('%s/%s.csv'%(folder_name9,str(speaker[2])), mode='r') as infile:
                        reader = csv.reader(infile)
                        for row in reader:
                           temp= row[0]
                           var2= temp.split('#+')
                           #print var2[0]
                           testDic[int(var2[0])]=row[0]
                        #print testDic
                except Exception as e:
                    print e
                '''
                tempStr=" "
                #retweets vs interNews
                print "retweets vs interNews"
                self.calValues(folder_name,folder_name_out,folder_name_out_p,speaker[2],tempStr)
                '''
                try:
                    tempX=list()
                    tempY=list()
                    count=0
                    if not os.path.exists(folder_name_out):
                        os.makedirs(folder_name_out)
                    with open('%s/%s.csv'%(folder_name,str(speaker[2])), mode='r') as infile:
                        reader = csv.reader(infile)
                        print speaker[2]
                        for row in reader:
                            temp= row[0]
                            var2= temp.split()
                            #print var2[0]
                            temp2=str(row[0]).replace(" ","#+")
                            d[int(var2[0])]=temp2+"#+"
                            #print(datetime.datetime.fromtimestamp(int("1284101485")).strftime('%Y-%m-%d %H:%M:%S'))
                            tempDate = datetime.fromtimestamp(int(var2[0])).strftime('%Y-%m-%d')
                            print tempDate
                            with open('%s/%s_corelation.txt'%(folder_name_out,str(speaker[2])), 'a+') as f3:
                                f3.write("%s\n" % (str(tempDate)))
                            tempX.insert(count,int(var2[1]))
                            tempY.insert(count,int(var2[2]))
                            count=count+1

                            #print tempX
                            #print tempY

                except Exception as e:
                    print e
                print count
                print tempX
                print tempY
                result=self.pearsonr(tempX,tempY)
                print "result-"
                print result
                #if not os.path.exists('%s/%s.txt'%(folderListFbUsers,"listFbUsers")):
                with open('%s/%s_corelation.txt'%(folder_name_out,str(speaker[2])), 'a+') as f3:
                    f3.write("Number of Days: %s\n%s\n%s\nResult: %s\n" % (str(count),str(tempX),str(tempY),str(result)))


                #print "retweets vs interNews"
                #self.calIntersect(speaker,d,testDic,folder_name_out,folder_name9)
                '''

                #retweets vs gt
                print "retweets vs gt"
                self.calValues(folder_name2,folder_name_out2,folder_name_out2_p,speaker[2],speaker[3])

                #fbLikes vs interNews
                print "fbLikes vs interNews"
                self.calValues(folder_name3,folder_name_out3,folder_name_out3_p,speaker[2],tempStr)

                #fbShare vs interNews
                print "fbShare vs interNews"
                self.calValues(folder_name4,folder_name_out4,folder_name_out4_p,speaker[2],tempStr)

                #sharecount
                #self.calValues(folder_name5,folder_name_out5,folder_name_out5_p,speaker[6],tempStr)

                #gt vs fbshares
                print "gt vs fbshares"
                self.calValues(folder_name6,folder_name_out6,folder_name_out6_p,speaker[2],tempStr)

                #retweets vs fbshare
                print "retweets vs fbshare"
                self.calValues(folder_name7,folder_name_out7,folder_name_out7_p,speaker[2],tempStr)

                #likeCount
                #self.calValues(folder_name8,folder_name_out8,folder_name_out8_p,speaker[6],tempStr)

                #gt vs interNews
                print "gt vs interNews"
                self.calValues(folder_name9,folder_name_out9,folder_name_out9_p,speaker[2],tempStr)

                #twfollowers vs interNews
                print "twfollowers vs interNews"
                self.calValues(folder_name10,folder_name_out10,folder_name_out10_p,speaker[2],tempStr)

                #twfollowers vs fblikes
                print "twfollowers vs fblikes"
                self.calValues(folder_name11,folder_name_out11,folder_name_out11_p,speaker[2],tempStr)

                #twfollowers vs fbShare
                print "twfollowers vs fbShare"
                self.calValues(folder_name12,folder_name_out12,folder_name_out12_p,speaker[2],tempStr)

                #gt vs fblikes
                print "gt vs fblikes"
                self.calValues(folder_name13,folder_name_out13,folder_name_out13_p,speaker[2],tempStr)

                #retweets vs fblikes
                print "retweets vs fblikes"
                self.calValues(folder_name14,folder_name_out14,folder_name_out14_p,speaker[2],tempStr)

            except Exception as e:
                print e

    def calValues(self,folder_name,folder_name_out,folder_name_out_p,speaker1,speaker2):
        d = {}

        try:
            tempX=list()
            tempY=list()
            count=0
            if not os.path.exists(folder_name_out):
                os.makedirs(folder_name_out)
            testFolder="crawled_reports"
            if testFolder in folder_name:
                pattern='%s%s(%s).csv'%(folder_name,str(speaker1),str(speaker2))
            else:
                pattern='%s/%s.csv'%(folder_name,str(speaker1))
            if os.path.exists(pattern):
                with open(pattern, mode='r') as infile:
                    reader = csv.reader(infile)
                    print speaker1
                    for row in reader:
                        temp= row[0]
                        var2= temp.split()
                        #print var2[0]
                        temp2=str(row[0]).replace(" ","#+")
                        d[int(var2[0])]=temp2+"#+"
                        #print(datetime.datetime.fromtimestamp(int("1284101485")).strftime('%Y-%m-%d %H:%M:%S'))
                        tempDate = datetime.fromtimestamp(int(var2[0])).strftime('%Y-%m-%d')
                        #print tempDate
                        with open('%s/%s_corelation.txt'%(folder_name_out,str(speaker1)), 'a+') as f3:
                            f3.write("%s\n" % (str(tempDate)))
                        tempX.insert(count,int(var2[1]))
                        tempY.insert(count,int(var2[2]))
                        count=count+1

                        #print tempX
                        #print tempY


            #print count
            #print tempX
            #print tempY
            #result=self.pearsonr(tempX,tempY)
            tempNewX=list()
            tempNewY=list()
            for index in range(len(tempX)):
                if index == 0:
                    tempNewX+= [(tempX[index] +tempX[index+1])/2.]
                    continue
                if index == len(tempX)-1:
                    tempNewX+= [(tempX[index] +tempX[index-1])/2.]
                    continue
                tempNewX+=  [(tempX[index+1]+  tempX[index] +tempX[index-1])/3.]

            for index2 in range(len(tempY)):
                if index2 == 0:
                    tempNewY+= [(tempY[index2] +tempY[index2+1])/2.]
                    continue
                if index2 == len(tempY)-1:
                    tempNewY+= [(tempY[index2] +tempY[index2-1])/2.]
                    continue
                tempNewY+=   [(tempY[index2+1]+  tempY[index2] +tempY[index2-1])/3.]

            result=self.pearsonr(tempNewX,tempNewY)
            #print "result-"
            #print result
            #if not os.path.exists('%s/%s.txt'%(folderListFbUsers,"listFbUsers")):
            with open('%s/%s_corelation.txt'%(folder_name_out,str(speaker1)), 'a+') as f3:
                #print '%s/%s_corelation.txt'%(folder_name_out,str(speaker1))
                f3.write("Number of Days: %s\n%s\n%s\nResult: %s\n" % (str(count),str(tempX),str(tempY),str(result)))

            with open('%s/all_corelation.txt'%(folder_name_out_p), 'a+') as f4:
                #print '%s/%s_corelation.txt'%(folder_name_out,str(speaker1))
                f4.write("Speaker: %s\nNumber of Days: %s\n%s\n%s\nResult: %s\n" % (str(speaker1),str(count),str(tempX),str(tempY),str(result)))

            with open('%s/all_corelation2.csv'%(folder_name_out_p), 'a+') as f5:
                #print '%s/%s_corelation.txt'%(folder_name_out,str(speaker1))
                #f5.write("%s#+%s#+%s#+%s#+%s\n" % (str(speaker1),str(count),str(tempX),str(tempY),str(result)))
                f5.write("%s#+%s#+%s\n" % (str(speaker1),str(count),str(result)))
        except Exception as e:
            print e

    def pearsonr(self,x, y):
        # Assume len(x) == len(y)
        n = len(x)
        sum_x = float(sum(x))
        sum_y = float(sum(y))
        sum_x_sq = sum(map(lambda x: pow(x, 2), x))
        sum_y_sq = sum(map(lambda x: pow(x, 2), y))
        psum = sum(imap(lambda x, y: x * y, x, y))
        num = psum - (sum_x * sum_y/n)
        den = pow((sum_x_sq - pow(sum_x, 2) / n) * (sum_y_sq - pow(sum_y, 2) / n), 0.5)
        if den == 0: return 0
        return num / den




