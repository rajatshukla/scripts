__author__ = 'shukla'

import logging
import db_handling as db_h
import os,sys,inspect
import time
from datetime import datetime
import csv,glob,subprocess

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
sys.path.insert(0,parentdir)


class GraphDataMergeFb:

#def __init__(self,twitter_keys,db_keys):
    def __init__(self,db_keys):
        self.token_list=[]
        #self.get_authentications(twitter_keys)
        self.handler = db_h.DBHandler(db_keys)

        self.logger = logging.getLogger('Scrape_InterCeder')
        fh=logging.FileHandler('interCeder_scrape.log')
        fh.setLevel(logging.DEBUG)
        self.logger.addHandler(fh)

    #function to rename txt to csv
    def rename(self,dir, pattern, titlePattern):
        for pathAndFilename in glob.iglob(os.path.join(dir, pattern)):
            title, ext = os.path.splitext(os.path.basename(pathAndFilename))
            os.rename(pathAndFilename,
                      #os.path.join(dir, titlePattern % title + ext))
                      os.path.join(dir, titlePattern % title))


    def getGraphDataMergeFb(self):

        speakers = self.handler.get_users_with_fullname_username()
        ts = int(time.time())

        for i,speaker in enumerate(speakers):

            try:
                #if ((speakers[4] != 'Debbie Harry/BLONDIE') or (speakers[4] != 'Dawn Richard / NEON') or (speakers[4]!='R. Dugdale/D. Petrie')):
                #folder_name = parentdir+'/scrape_intraceder/interceder_reports/%s'%speaker[2]
                #retweets vs interNews
                folder_name = parentdir+'/crawl_newsCred/allGraphData/interceder/graph_data/%s'%speaker[2]
                #retweets vs gt
                folder_name2 = parentdir+'/crawl_newsCred/allGraphData/gtRetweets/crawled_reports/'
                #fbLikes vs interNews
                folder_name3=parentdir+'/crawl_newsCred/allGraphData/interceder/graph_data_fbInterceder/%s'%speaker[2]
                #fbShare vs interNews
                folder_name4=parentdir+'/crawl_newsCred/allGraphData/interceder/graph_data_fbShare_Interceder/%s'%speaker[2]

                #sharecount
                folder_name5=parentdir+'/crawl_newsCred/allGraphData/fb/fb_reports/%s'%speaker[6]
                #gt vs fbshares
                folder_name6=parentdir+'/crawl_newsCred/allGraphData/fb/graph_data_fbShare_Gt/%s'%speaker[2]
                #retweets vs fbshare
                folder_name7=parentdir+'/crawl_newsCred/allGraphData/fb/graph_data_fbShare_Retweets/%s'%speaker[2]
                #likeCount
                folder_name8=parentdir+'/crawl_newsCred/allGraphData/fb/FinalLikeResults/%s'%speaker[6]
                #articles
                folder_name9=parentdir+'/crawl_newsCred/allGraphData/fb/fb_reports/%s'%speaker[6]
                #twfollowers vs fblike
                folder_name10 = parentdir+'/crawl_newsCred/allGraphData/fb/graph_data_fbLike_twfollowers/%s'%speaker[2]
                #twfollowers vs fbShare
                folder_name11 = parentdir+'/crawl_newsCred/allGraphData/fb/graph_data_fbShare_twfollowers/%s'%speaker[2]


                #output merged results
                #retweets vs interNews
                folder_name_out = parentdir+'/crawl_newsCred/allGraphData/interceder/fbP_graph_data/%s'%speaker[2]
                #retweets vs gt
                folder_name_out2 = parentdir+'/crawl_newsCred/allGraphData/gtRetweets/fbP_crawled_reports/'
                #fbLikes vs interNews
                folder_name_out3=parentdir+'/crawl_newsCred/allGraphData/interceder/fbP_graph_data_fbInterceder/%s'%speaker[2]
                #fbShare vs interNews
                folder_name_out4=parentdir+'/crawl_newsCred/allGraphData/interceder/fbP_graph_data_fbShare_Interceder/%s'%speaker[2]
                #sharecount
                folder_name_out5=parentdir+'/crawl_newsCred/allGraphData/fb/fbP_fb_reports/%s'%speaker[6]
                #gt vs fbshares
                folder_name_out6=parentdir+'/crawl_newsCred/allGraphData/fb/fbP_graph_data_fbShare_Gt/%s'%speaker[2]
                #retweets vs fbshare
                folder_name_out7=parentdir+'/crawl_newsCred/allGraphData/fb/fbP_graph_data_fbShare_Retweets/%s'%speaker[2]
                #likeCount
                folder_name_out8=parentdir+'/crawl_newsCred/allGraphData/fb/fbP_FinalLikeResults/%s'%speaker[6]
                #fbLike vs twfollowers
                folder_name_out9 = parentdir+'/crawl_newsCred/allGraphData/fb/fbP_graph_data_fbLike_twfollowers/%s'%speaker[2]
                #fbShare vs twfollowers
                folder_name_out10 = parentdir+'/crawl_newsCred/allGraphData/fb/fbP_graph_data_fbShare_twfollowers/%s'%speaker[2]

                d = {}
                testDic={}
                #print'%s/%s_postDetails.csv'%(folder_name9,str(speaker[6]))
                try:
                    with open('%s/%s_postDetailsMessage.csv'%(folder_name9,str(speaker[6])), mode='r') as infile:

                        reader = csv.reader(infile)
                        for row in reader:
                           temp= row[0]
                           #print "row"
                           print row
                           var2= temp.split("#+")
                           #print "var2[0]"
                           #print var2[0]
                           testDic[int(var2[0])]=row[0]
                        #print "testDic-"+testDic
                except Exception as e:
                    print e

                d1={}
                #retweets vs interNews
                try:
                    with open('%s/%s.csv'%(folder_name,str(speaker[2])), mode='r') as infile:
                        reader = csv.reader(infile)
                        for row in reader:
                            temp= row[0]
                            var2= temp.split()
                            #print var2[0]
                            temp2=str(row[0]).replace(" ","#+")
                            try:
                                if isinstance( int(var2[0]), int ):
                                    d1[int(var2[0])]=temp2+"#+"
                            except TypeError as error1:
                                print error1



                except Exception as e:
                    print e

                print "retweets vs interNews"
                #print "testDic"
                #print testDic
                self.calIntersect(speaker[2],speaker[6],d1,testDic,folder_name_out,folder_name9)

                d2={}
                #retweets vs gt
                try:
                    with open('%s%s(%s).csv'%(folder_name2,str(speaker[2]),str(speaker[3])), mode='r') as infile:
                        reader = csv.reader(infile)
                        for row in reader:
                            temp= row[0]
                            var2= temp.split()
                            #print var2[0]
                            '''
                            #check for d not testdic
                            if str(var2[2]) == "":
                                var2[2]='0'
                            if str(var2[1]) == "":
                                var2[1]='0'
                            '''
                            temp2=str(row[0]).replace(" ","#+")
                            try:
                                if isinstance( int(var2[0]), int ):
                                    d2[int(var2[0])]=temp2+"#+"
                            except TypeError as error1:
                                print error1

                except Exception as e:
                    print e

                print "retweets vs gt"
                self.calIntersect(speaker[2],speaker[6],d2,testDic,folder_name_out2,folder_name9)

                d3={}
                #fbLikes vs interNews
                try:
                    with open('%s/%s.csv'%(folder_name3,str(speaker[2])), mode='r') as infile:
                        reader = csv.reader(infile)
                        for row in reader:
                            temp= row[0]
                            var2= temp.split()
                            #print var2[0]
                            temp2=str(row[0]).replace(" ","#+")
                            try:
                                if isinstance( int(var2[0]), int ):
                                    d3[int(var2[0])]=temp2+"#+"
                            except TypeError as error1:
                                print error1


                except Exception as e:
                    print e

                print "fbLikes vs interNews"
                self.calIntersect(speaker[2],speaker[6],d3,testDic,folder_name_out3,folder_name9)

                d4={}
                #fbShare vs interNews
                try:
                    with open('%s/%s.csv'%(folder_name4,str(speaker[2])), mode='r') as infile:
                        reader = csv.reader(infile)
                        for row in reader:
                            temp= row[0]
                            var2= temp.split()
                            #print var2[0]
                            temp2=str(row[0]).replace(" ","#+")
                            try:
                                if isinstance( int(var2[0]), int ):
                                    d4[int(var2[0])]=temp2+"#+"
                            except TypeError as error1:
                                print error1


                except Exception as e:
                    print e

                print "fbShare vs interNews"
                self.calIntersect(speaker[2],speaker[6],d4,testDic,folder_name_out4,folder_name9)

                d5={}
                #sharecount
                try:
                    with open('%s/%s.csv'%(folder_name5,str(speaker[6])), mode='r') as infile:
                        reader = csv.reader(infile)
                        for row in reader:
                            temp= row[0]
                            var2= temp.split()
                            #print var2[0]
                            temp2=str(row[0]).replace(" ","#+")
                            try:
                                if isinstance( int(var2[0]), int ):
                                    d5[int(var2[0])]=temp2+"#+"
                            except TypeError as error1:
                                print error1


                except Exception as e:
                    print e

                print "sharecount"
                self.calIntersect(speaker[6],speaker[6],d5,testDic,folder_name_out5,folder_name9)

                d6={}
                #gt vs fbshares
                try:
                    with open('%s/%s.csv'%(folder_name6,str(speaker[2])), mode='r') as infile:
                        reader = csv.reader(infile)
                        for row in reader:
                            temp= row[0]
                            var2= temp.split()
                            #print var2[0]
                            temp2=str(row[0]).replace(" ","#+")
                            try:
                                if isinstance( int(var2[0]), int ):
                                    d6[int(var2[0])]=temp2+"#+"
                            except TypeError as error1:
                                print error1


                except Exception as e:
                    print e

                print "gt vs fbshares"
                self.calIntersect(speaker[2],speaker[6],d6,testDic,folder_name_out6,folder_name9)

                d7={}
                #retweets vs fbshare
                try:
                    with open('%s/%s.csv'%(folder_name7,str(speaker[2])), mode='r') as infile:
                        reader = csv.reader(infile)
                        for row in reader:
                            temp= row[0]
                            var2= temp.split()
                            #print var2[0]
                            temp2=str(row[0]).replace(" ","#+")
                            try:
                                if isinstance( int(var2[0]), int ):
                                    d7[int(var2[0])]=temp2+"#+"
                            except TypeError as error1:
                                print error1
                        #print testDic

                except Exception as e:
                    print e

                print "gt vs fbshares"
                self.calIntersect(speaker[2],speaker[6],d7,testDic,folder_name_out7,folder_name9)

                d8={}
                #likeCount
                try:
                    with open('%s/%s.csv'%(folder_name8,str(speaker[6])), mode='r') as infile:
                        reader = csv.reader(infile)
                        for row in reader:
                            temp= row[0]
                            var2= temp.split()
                            #print var2[0]
                            temp2=str(row[0]).replace(" ","#+")
                            try:
                                if isinstance( int(var2[0]), int ):
                                    d8[int(var2[0])]=temp2+"#+"
                            except TypeError as error1:
                                print error1


                except Exception as e:
                    print e

                print "likeCount"
                self.calIntersect(speaker[6],speaker[6],d8,testDic,folder_name_out8,folder_name9)

                d9={}
                #retweets vs interNews
                try:
                    with open('%s/%s.csv'%(folder_name10,str(speaker[2])), mode='r') as infile:
                        reader = csv.reader(infile)
                        for row in reader:
                            temp= row[0]
                            var2= temp.split()
                            #print var2[0]
                            temp2=str(row[0]).replace(" ","#+")
                            try:
                                if isinstance( int(var2[0]), int ):
                                    d9[int(var2[0])]=temp2+"#+"
                            except TypeError as error1:
                                print error1


                except Exception as e:
                    print e

                print "fblike vs twfollowers"
                self.calIntersect(speaker[2],speaker[6],d9,testDic,folder_name_out9,folder_name9)

                d10={}
                #retweets vs interNews
                try:
                    with open('%s/%s.csv'%(folder_name11,str(speaker[2])), mode='r') as infile:
                        reader = csv.reader(infile)
                        for row in reader:
                            temp= row[0]
                            var2= temp.split()
                            #print var2[0]
                            temp2=str(row[0]).replace(" ","#+")
                            try:
                                if isinstance( int(var2[0]), int ):
                                    d10[int(var2[0])]=temp2+"#+"
                            except TypeError as error1:
                                print error1


                except Exception as e:
                    print e

                print "fbShare vs twfollowers"
                self.calIntersect(speaker[2],speaker[6],d10,testDic,folder_name_out10,folder_name9)

            except Exception as e:
                print e

    def calIntersect(self,speaker1,speaker2,d,testDic,folder_name_out,folder_name9):
        '''
        for k in testDic.keys():
            if testDic[k]==d[k]:
               print k,testDic[k]
        '''
        '''
        for key in d.keys() & testDic.keys():
            print d[key], testDic[key]
        '''
        print "Results for- %s"%speaker1
        set1=set(d.keys())
        k1=d.keys()
        k1.sort()
        print k1
        set2=set(testDic.keys())
        k2=testDic.keys()
        k2.sort()
        print k2
        #print set1.intersection(set2)
        #print set1.union(set2)
        set3=set1.intersection(set2)
        #print set3
        set3=sorted(set3)
        #print set3
        if not os.path.exists(folder_name_out):
            os.makedirs(folder_name_out)
        res1=''
        for cnt1 in set3:
            #print cnt1, d[cnt1], testDic[cnt1]
            #writing the finale results timestamp,news views and retweet count

            res1+="%s %s\n" % (str(d[cnt1]),str(testDic[cnt1]))

        #'a+' using append in csv it does not write results in sequence of input

            with open('%s/%s.txt'%(folder_name_out,str(speaker1)), 'a+') as f:
                #if str(speaker[2]) == "HillaryClinton":
                    #print cnt1
                try:
                    with open('%s/%s_postDetailsMessage.csv'%(folder_name9,str(speaker2)), mode='r') as infile:
                        reader = csv.reader(infile)
                        for row in reader:
                            temp= row[0]
                            var2= temp.split("#+")
                            #print var2[0]
                            testDic[int(var2[0])]=row[0]

                            #print "testDic"
                            #print testDic
                            #logic to get date from timestamp
                            tempDate=datetime.fromtimestamp(int(var2[0])).strftime('%Y-%m-%d')
                            #print "tempDate"+tempDate
                            if str(var2[0]) in str(cnt1):
                                f.write("%s#+%s#+%s\n" % (str(tempDate),str(d[cnt1]), str(testDic[cnt1])))
                                #f.write("%s %s %s\n" % (str(tempDate),str(d[cnt1]), str(testDic[cnt1])))

                except Exception as e:
                    print e

                #f.write("%s %s\n" % (str(d[cnt1]), str(testDic[cnt1])))

        '''
        #writes everything in file but heavy on memory
        #print res1
        with open('%s/%s.csv'%(folder_name_out,str(speaker[2])), 'w') as f2:
            f2.write(res1)

        '''

        #as it is part of a function and not called from body of class as in other programs
        self.rename(r'%s'%folder_name_out, r'*.txt', r'%s.csv')







