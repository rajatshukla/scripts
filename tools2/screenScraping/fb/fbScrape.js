// Using Casperjs to screenshot an entire Facebook Post */
var casper = require('casper').create({
  clientScripts:  [
    'jquery.js'
  ]
});

//login to facebook
casper.start('https://www.facebook.com/', function() {    
  this.fill('#login_form', { email: 'email', pass: 'password' }, true);    
});

//go to the facebook post
var thePost = "https://www.facebook.com/jonaslund2000/posts/10150535403965904";
casper.thenOpen(thePost, function() {
  console.log("At the right post");
});

//start loop
casper.then(function() {
  viewMore(0);
});

//click view more posts, make screenshot
function viewMore(i) {  
  casper.then(function() {
    //remove last 50 comments to keep the RAM fresh
    this.evaluate(function() {
      jQuery(function($) { 
        $("ul.commentList > li").slice(-50).remove();
      });
    });

    //view more posts
    this.click("input.stat_elem");
    console.log("clicked view more – now capturing");
    
    //echo where we are
    this.echo(this.fetchText('.ufiCommentCount'));
    
    //wait for the comments to load
    this.wait(5000, function() {
       this.echo(this.fetchText('.ufiCommentCount'));               
       
       //capture – config the clipRect to your liking!
       this.capture(+i+'.png', {top:634, left:0, width: 689, height: 2233});
       this.echo("captured:" + i);

       //start over, take total amount of comments divided by 50 to get the stop
       if(i < 25000) {
         viewMore(i+1);
       }
    });
  });
}

//run
casper.run();
