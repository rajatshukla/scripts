__author__ = 'shukla'

import credentials.token as t
import users.speaker as spk

import logging

import db_handling as db_h

import os,sys,inspect
import time
from subprocess import call
import random
from datetime import datetime
import csv,glob

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
sys.path.insert(0,parentdir)
import db_handling as han




class GraphDataFbShareTwFollowers:

#def __init__(self,twitter_keys,db_keys):
    def __init__(self,db_keys):
        self.token_list=[]
        #self.get_authentications(twitter_keys)
        self.handler = db_h.DBHandler(db_keys)

        #self.logger = logging.getLogger('Scrape_InterCeder')
        #fh=logging.FileHandler('interCeder_scrape.log')
        #fh.setLevel(logging.DEBUG)
        #self.logger.addHandler(fh)

    #function to rename txt to csv
    def rename(self,dir, pattern, titlePattern):
        for pathAndFilename in glob.iglob(os.path.join(dir, pattern)):
            title, ext = os.path.splitext(os.path.basename(pathAndFilename))
            os.rename(pathAndFilename,
                      #os.path.join(dir, titlePattern % title + ext))
                      os.path.join(dir, titlePattern % title))

    def getFbShareTwFollowersGraphData(self):

        speakers = self.handler.get_users_with_fullname_username()
        ts = int(time.time())

        for i,speaker in enumerate(speakers):

            try:
                #if ((speakers[4] != 'Debbie Harry/BLONDIE') or (speakers[4] != 'Dawn Richard / NEON') or (speakers[4]!='R. Dugdale/D. Petrie')):
                #folder_name = parentdir+'/scrape_intraceder/interceder_reports/%s'%speaker[2]
                folder_name2 = parentdir+'/scrape_intraceder/mergeResults/%s'%speaker[2]
                folder_name = parentdir+'/crawl_facebook/crawled_reports/'
                folder_name3 = parentdir+'/crawl_facebook/graph_data_fbShare_twfollowers/%s'%speaker[2]

                d = {}
                testDic={}
                #reading scraped results from interceder
                #if os.path.isfile('%s/%s'%(folder_name,str(speaker[2]))):
                with open('%s%s(%s).csv'%(folder_name,str(speaker[2]),str(speaker[3])), mode='r') as infile:
                    reader = csv.reader(infile)
                    for row in reader:
                       temp= row[0]
                       var2= temp.split()
                       #print var2[0]
                       d[int(var2[0])]=var2[3]
                    #print testDic


                #if os.path.isfile('%s%s(google_trends).csv'%(folder_name2,str(speaker[2]))):
                    '''
                    with open("AdrianPeterson(google_trends).csv") as f:
                        for line in f:
                           (key, val) = line.split()
                           testDic[int(k)] = val
                        print testDic
                    '''
                    #reading crawled results
                folderFinalUserName = parentdir+'/crawl_facebook/fb_reports/%s'%str(speaker[6])
                with open('%s/%s.csv'%(folderFinalUserName,str(speaker[6])), mode='r') as infile:
                    reader = csv.reader(infile)
                    for row in reader:
                       temp= row[0]
                       var2= temp.split()
                       #print var2[0]
                       testDic[int(var2[0])]=var2[1]
                    #print testDic

                '''
                for k in testDic.keys():
                    if testDic[k]==d[k]:
                       print k,testDic[k]
                '''
                '''
                for key in d.keys() & testDic.keys():
                    print d[key], testDic[key]
                '''
                print "Results for- %s"%speaker[2]
                set1=set(d.keys())
                k1=d.keys()
                k1.sort()
                #print k1
                set2=set(testDic.keys())
                k2=testDic.keys()
                k2.sort()
                #print k2
                #print set1.intersection(set2)
                #print set1.union(set2)
                set3=set1.intersection(set2)
                #print set3
                set3=sorted(set3)
                #print set3
                if not os.path.exists(folder_name3):
                    os.makedirs(folder_name3)
                res1=''
                for cnt1 in set3:
                    #print cnt1, d[cnt1], testDic[cnt1]
                    #writing the finale results timestamp,news views and retweet count
                    #TODO-rajat sort the list according to timestamp
                    #res1+="%s %s %s\n" % ((cnt1),str(d[cnt1]),str(testDic[cnt1]))
                    with open('%s/%s.txt'%(folder_name3,str(speaker[2])), 'a+') as f:
                            #if str(speaker[2]) == "HillaryClinton":
                                #print cnt1
                            try:
                                with open('%s/%s.csv'%(folderFinalUserName,str(speaker[6])), mode='r') as infile:
                                    reader = csv.reader(infile)
                                    #to get all news articles for same date
                                    for row in reader:
                                        temp= row[0]
                                        var2= temp.split()
                                        #print var2[0]
                                        testDic[int(var2[0])]=var2[1]
                                        #tempDate = datetime.fromtimestamp(int(var2[0])).strftime('%Y-%m-%d')+"#+"
                                        if str(var2[0]) in str(cnt1):
                                            #print cnt1
                                            #print str(d[cnt1])
                                            #print str(testDic[cnt1])
                                            #f.write("%s %s %s\n" % (str(tempDate),str(d[cnt1]), str(testDic[cnt1])))
                                            f.write("%s %s %s\n" % ((cnt1),str(d[cnt1]),str(testDic[cnt1])))

                            except Exception as e:
                                print e

                self.rename(r'%s'%folder_name3, r'*.txt', r'%s.csv')
                '''
                #'a+' using append in csv it does not write results in sequence of input

                    with open('%s/%s'%(folder_name3,str(speaker[2])), 'a+') as f:
                        #if str(speaker[2]) == "HillaryClinton":
                            #print cnt1
                        f.write("%s %s %s\n" % ((cnt1),str(d[cnt1]), str(testDic[cnt1])))

                temp1=parentdir+'/scrape_intraceder/graph_data/%s'%str(speaker[2])+'/%s'%(str(speaker[2]))
                temp2=parentdir+'/scrape_intraceder/graph_data/%s'%str(speaker[2])+'/%s.csv'%(str(speaker[2]))
                print temp1
                print temp2
                call(['mv temp1 temp2'])
                #os.rename(temp1,temp2)
                '''
                #'''
                #writes everything in file but heavy on memory
                #print res1
                #with open('%s/%s.csv'%(folder_name3,str(speaker[2])), 'w') as f2:
                    #f2.write(res1)

                #'''

            except Exception as e:
                print e

