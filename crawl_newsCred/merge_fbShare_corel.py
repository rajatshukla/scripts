__author__ = 'shukla'

import credentials.token as t
import users.speaker as spk

import logging

import db_handling as db_h

import os,sys,inspect
import time
from subprocess import call
import random
from datetime import datetime
import csv,glob

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
sys.path.insert(0,parentdir)
import db_handling as han




class MergeDataFbShareColrel:

#def __init__(self,twitter_keys,db_keys):
    def __init__(self,db_keys):
        self.token_list=[]
        #self.get_authentications(twitter_keys)
        self.handler = db_h.DBHandler(db_keys)

        #self.logger = logging.getLogger('Scrape_InterCeder')
        #fh=logging.FileHandler('interCeder_scrape.log')
        #fh.setLevel(logging.DEBUG)
        #self.logger.addHandler(fh)



    def getMergeFbShareColrel(self):

        speakers = self.handler.get_users_with_fullname_username()
        ts = int(time.time())



        #if ((speakers[4] != 'Debbie Harry/BLONDIE') or (speakers[4] != 'Dawn Richard / NEON') or (speakers[4]!='R. Dugdale/D. Petrie')):
        #folder_name = parentdir+'/scrape_intraceder/interceder_reports/%s'%speaker[2]
        #folder_name = parentdir+'/crawl_facebook/crawled_reports/'
        folder_name = parentdir+'/crawl_newsCred/allGraphData/fb/corelation_graph_data_fbShare_twfollowers/all_corelation2.csv'
        folder_name2 = parentdir+'/crawl_newsCred/allGraphData/fb/corelation_graph_data_fbShare_Gt/all_corelation2.csv'
        folder_name3 = parentdir+'/crawl_newsCred/allGraphData/fb/corelation_graph_data_fbShare_Retweets/all_corelation2.csv'

        folderMergeFbShareCorel=parentdir+'/crawl_newsCred/allGraphData/fb/corelation_merge_fbShare_all'

        d1={}
        try:
            if os.path.exists(folder_name):
                with open('%s'%(folder_name), mode='r') as infile:
                    reader = csv.reader(infile)
                    for row in reader:
                        temp= row[0]
                        var2= temp.split('#+')
                        #print var2[0]
                        #temp2=str(row[0]).replace(" ","#+")
                        temp2=str(row[0])
                        d1[str(var2[0])]=var2[2]
        except Exception as e:
            print e

        d2={}
        try:
            if os.path.exists(folder_name2):
                with open('%s'%(folder_name2), mode='r') as infile:
                    reader = csv.reader(infile)
                    for row in reader:
                        temp= row[0]
                        var2= temp.split('#+')
                        #print var2[0]
                        #temp2=str(row[0]).replace(" ","#+")
                        temp2=str(row[0])
                        d2[str(var2[0])]=var2[2]
        except Exception as e:
            print e

        d3={}
        try:
            if os.path.exists(folder_name3):
                with open('%s'%(folder_name3), mode='r') as infile:
                    reader = csv.reader(infile)
                    for row in reader:
                        temp= row[0]
                        var2= temp.split('#+')
                        #print var2[0]
                        #temp2=str(row[0]).replace(" ","#+")
                        temp2=str(row[0])
                        d3[str(var2[0])]=var2[2]
        except Exception as e:
            print e



        if not os.path.exists('%s'%(folderMergeFbShareCorel)):
            os.makedirs(folderMergeFbShareCorel)

        #for key in d1:
        for i,speaker in enumerate(speakers):
            if speaker[2] not in d1.keys():
                d1[str(speaker[2])]="NA"
            if speaker[2] not in d2.keys():
                d2[str(speaker[2])]="NA"
            if speaker[2] not in d3.keys():
                d3[str(speaker[2])]="NA"

            try:
                print speaker[2]
                print speaker[3]
                print "d1[speaker[2]]",d1[speaker[2]]
                print "d2[speaker[2]]",d2[speaker[2]]
                print "d3[speaker[2]]",d3[speaker[2]]


                with open('%s/%s.csv'%(folderMergeFbShareCorel,"all_corelation_fbShare"), 'a+') as f3:
                    f3.write("%s %s %s %s %s \n" % (speaker[2],speaker[3],d1[speaker[2]],d2[speaker[2]],d3[speaker[2]]))
            except Exception as e:
                print e
