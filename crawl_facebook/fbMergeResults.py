#help resource
#http://nbviewer.ipython.org/github/ptwobrussell/Mining-the-Social-Web-2nd-Edition/blob/master/ipynb/Chapter%202%20-%20Mining%20Facebook.ipynb

# Copy and paste in the value you just got from the inline frame into this variable and execute this cell.
# Keep in mind that you could have just gone to https://developers.facebook.com/tools/access_token/
# and retrieved the "User Token" value from the Access Token Tool

ACCESS_TOKEN = ''
import facebook # pip install facebook-sdk
import json
from pprint import pprint
import logging
#import db_handling as db_h
import os,sys,inspect,glob
import time
from subprocess import call
import random
from datetime import datetime
import csv

#setting path for parent directory
parentdir = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
sys.path.insert(0,parentdir)

class CrawlFacebook:

    def __init__(self):
        return ""

    #function to rename txt to csv
    def rename(dir, pattern, titlePattern):
        for pathAndFilename in sorted(glob.iglob(os.path.join(dir, pattern))):
            title, ext = os.path.splitext(os.path.basename(pathAndFilename))
            os.rename(pathAndFilename,
                      #os.path.join(dir, titlePattern % title + ext))
                      os.path.join(dir, titlePattern % title))

            #'''
    #function to merge txt to csv
    def merge(dir, pattern, titlePattern):
        print dir,pattern,titlePattern
        folderMerge = parentdir+'/crawl_facebook/likeCount/mergeLike'
        if not os.path.exists(folderMerge):
            os.makedirs(folderMerge)
            for pathAndFilename in sorted(glob.iglob(os.path.join(dir, pattern))):
                title, ext = os.path.splitext(os.path.basename(pathAndFilename))
                print title + ext
                print pathAndFilename
                '''
                os.rename(pathAndFilename,
                          #os.path.join(dir, titlePattern % title + ext))
                          os.path.join(dir, titlePattern % title))
                '''


                with open('%s'%(pathAndFilename), mode='r') as infile1:
                    reader1 = csv.reader(infile1)
                    for row1 in reader1:
                        temp1= row1[0]

                        #writing page details in file
                        with open('%s/%s.txt'%(folderMerge,str("MergeResults")), 'a+') as f2:

                            #f2.write("%s %s %s %s %s \n" % (str(obj["id"]),str(obj["username"]),str(obj["likes"]),str(obj["talking_about_count"]),str(obj["checkins"])))
                            f2.write("%s \n"%(str(row1[0])))
                    #print titlePattern % title + ext


    folder_name3 = parentdir+'/crawl_facebook/likeCount/fb_users/*'
    #renaming txt to csv
    print "hello"
    merge(r'%s'%folder_name3, r'*.csv', r'%s.csv')

    folderMerge = parentdir+'/crawl_facebook/likeCount/mergeLike'
    rename(r'%s'%folderMerge, r'*.txt', r'%s.csv')

    fileFbIds= parentdir+'/crawl_facebook/fbPageIds.csv'
    with open('%s'%(fileFbIds), mode='r') as infile1:
        reader1 = csv.reader(infile1)
        for row1 in reader1:
            temp1= row1[0]
            tempId= temp1.split(';')
            #print tempId[0]
            #mtsw_id = '67253243887'
            pageUserName = str(tempId[1])
            print pageUserName
            folderFinalUserName = parentdir+'/crawl_facebook/likeCount/FinalLikeResults/%s'%pageUserName
            if not os.path.exists(folderFinalUserName):
                os.makedirs(folderFinalUserName)

                #testDic2={}
                with open('%s/MergeResults.csv'%(folderMerge), mode='r') as infile1:
                    reader1 = csv.reader(infile1)
                    count=1
                    testDic={}
                    for row1 in reader1:
                        temp1= row1[0]
                        tempId= temp1.split(' ')
                        tempRes=0
                        #print tempId[0]
                        #mtsw_id = '67253243887'
                        pageUserNameMergeFile = str(tempId[3])
                        timeSt= str(tempId[1])
                        likeCount= str(tempId[4])

                        if pageUserName in [pageUserNameMergeFile]:
                            #testDic[count]=str(tempId[1])
                            testDic.setdefault(count,[]).append(str(tempId[1]))
                            testDic.setdefault(count,[]).append(str(tempId[4]))
                            #print testDic
                            #testDic2[count]=tempId[1]
                            count=count+1
                            #writing user details in file
                            with open('%s/%s_PageDetails.txt'%(folderFinalUserName,str(pageUserName)), 'a+') as f2:
                                f2.write("%s \n"%(str(row1[0])))
                            with open('%s/%s_LikeCounts.txt'%(folderFinalUserName,str(pageUserName)), 'a+') as f2:
                                f2.write("%s %s\n"%(str(timeSt),str(likeCount)))

                    print testDic
                    for key in testDic:
                        #print testDic[key][1]
                        with open('%s/%s.txt'%(folderFinalUserName,str(pageUserName)), 'a+') as f2:
                            incKey=key+1
                            try:
                                if(testDic[incKey]):
                                    tempRes=int(testDic[incKey][1])-int(testDic[key][1])
                                    f2.write("%s %s\n"%(str(testDic[key][0]),str(tempRes)))
                            except Exception,e:
                                print str(e)

            rename(r'%s'%folderFinalUserName, r'*.txt', r'%s.csv')
