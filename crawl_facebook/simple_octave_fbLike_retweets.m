arg_list = argv();
arg_list{4}
data = load(arg_list{4});
data
#takes as arguments the source csv file , the desired dest jpg file and username which it puts as title in plot
seconds_per_day=60*60*24;
data(:,1)=datenum('1970','yyyy') + data(:,1)/seconds_per_day;
[ax h1 h2]=plotyy(data(:,1),data(:,3),data(:,1),data(:,2));
set(h1, 'LineWidth',4);
set(h2, 'LineWidth',4);
axes(ax(1)); xlabel('Time'); ylabel('FbLikes');
datetick('x','dd-mmm','keepticks');
axes(ax(2)); ylabel('retweets');
datetick('x','dd-mmm','keepticks');
xlabel('Time','FontSize',25);
title(arg_list{6},'FontSize',30);

%activate major and minor grid
grid on;
grid minor;

print (arg_list{5});
