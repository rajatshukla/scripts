#help resource
#http://nbviewer.ipython.org/github/ptwobrussell/Mining-the-Social-Web-2nd-Edition/blob/master/ipynb/Chapter%202%20-%20Mining%20Facebook.ipynb

# Copy and paste in the value you just got from the inline frame into this variable and execute this cell.
# Keep in mind that you could have just gone to https://developers.facebook.com/tools/access_token/
# and retrieved the "User Token" value from the Access Token Tool

ACCESS_TOKEN = ''
import facebook # pip install facebook-sdk
import json
from pprint import pprint
import logging
#import db_handling as db_h
import os,sys,inspect,glob
import time
from subprocess import call
import random
from datetime import datetime
import csv

#setting path for parent directory
parentdir = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
sys.path.insert(0,parentdir)

class CrawlFacebook:

    # A helper function to pretty-print Python objects as JSON
    def pp(o):
        print json.dumps(o, indent=1)

    #function to rename txt to csv
    def rename(dir, pattern, titlePattern):
        for pathAndFilename in glob.iglob(os.path.join(dir, pattern)):
            title, ext = os.path.splitext(os.path.basename(pathAndFilename))
            os.rename(pathAndFilename,
                      #os.path.join(dir, titlePattern % title + ext))
                      os.path.join(dir, titlePattern % title))

    # Create a connection to the Graph API with your access token
    g = facebook.GraphAPI(ACCESS_TOKEN)

    '''
    # Execute a few sample queries

    print '---------------'
    print 'Me'
    print '---------------'
    #pp(g.get_object('me'))
    print
    print '---------------'
    print 'My Friends'
    print '---------------'
    #pp(g.get_connections('me', 'friends'))
    print
    print '---------------'
    print 'Social Web'
    print '---------------'
    #pp(g.request("search", {'q' : 'social web', 'type' : 'page'}))
    '''

    fileFbIds= parentdir+'/crawl_facebook/fbPageIds.csv'
    count=0
    # Get an instance of Mining the Social Web
    # Using the page name also works if you know it.
    # e.g. 'MiningTheSocialWeb' or 'CrossFit'
    #page_id
    #reading facebook ids
    with open('%s'%(fileFbIds), mode='r') as infile1:
        reader1 = csv.reader(infile1)
        for row1 in reader1:
            temp1= row1[0]
            tempId= temp1.split(';')
            #print tempId[0]
            #mtsw_id = '67253243887'
            mtsw_id = str(tempId[0])
            count=count+1

            '''
            pageId/posts- https://developers.facebook.com/docs/graph-api/reference/page
            edges in page means using page_id what other info, like posts  you can get,
            other fields(without /) shows properties for page id like 'id','name','username'
            later, try for other than page id....
            '''

            #properties of page
            #pp(g.get_object(mtsw_id))
            obj=g.get_object(mtsw_id)
            print "username:"
            pprint(obj["username"])
            '''
            try:
                print "Page Id:"
                pprint(obj["id"])
                print "name:"
                pprint(obj["name"])
                print "username:"
                pprint(obj["username"])
                print "likes:"
                pprint(obj["likes"])
                print "talking_about_count:"
                pprint(obj["talking_about_count"])
                print "checkins:"
                pprint(obj["checkins"])
            except Exception:
                print Exception
            '''
            #calculate today timestamp
            today=datetime.strptime(str(datetime.now()), "%Y-%m-%d %H:%M:%S.%f").date()
            todayTs=time.mktime(datetime.utctimetuple(datetime.strptime(str(today),"%Y-%m-%d")))+3600
            todayTs=int(todayTs)
            #print int(key)

            #setting folder path
            #folder_name3 = parentdir+'/crawl_facebook/graph_data/%s'%speaker[2]
            folder_name3 = parentdir+'/crawl_facebook/likeCount/fb_reports/%d'%todayTs
            folderListFbUsers = parentdir+'/crawl_facebook/likeCount/fb_users/%d'%todayTs

            if not os.path.exists(folder_name3):
                os.makedirs(folder_name3)

            if not os.path.exists(folderListFbUsers):
                os.makedirs(folderListFbUsers)

            if not os.path.exists('%s/%s.txt'%(folder_name3,str(obj["username"])+"_PageDetails")):
                #writing page details in file
                with open('%s/%s.txt'%(folder_name3,str(obj["username"])+"_PageDetails"), 'w') as f2:
                    #f2.write("%s %s %s %s %s %s\n" % (str(obj["id"]),str(obj["name"]),str(obj["username"]),str(obj["likes"]),str(obj["talking_about_count"]),str(obj["checkins"])))
                    f2.write("%s %s %s %s %s %s \n" % (str(todayTs),str(obj["id"]),str(obj["username"]),str(obj["likes"]),str(obj["talking_about_count"]),str(obj["checkins"])))

            #number of users crawled
            #if not os.path.exists('%s/%s.txt'%(folderListFbUsers,"listFbUsers")):
            with open('%s/%s.txt'%(folderListFbUsers,"listFbUsers"), 'a+') as f3:
                #f3.write("%s %s %s %s %s %s %s\n" % (str(count),str(obj["id"]),str(obj["name"]),str(obj["username"]),str(obj["likes"]),str(obj["talking_about_count"]),str(obj["checkins"])))
                f3.write("%s %s %s %s %s %s %s \n" % (str(count),str(todayTs),str(obj["id"]),str(obj["username"]),str(obj["likes"]),str(obj["talking_about_count"]),str(obj["checkins"])))



            #renaming txt to csv
            rename(r'%s'%folder_name3, r'*.txt', r'%s.csv')
        rename(r'%s'%folderListFbUsers, r'*.txt', r'%s.csv')
