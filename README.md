Scripts that can be reused for jump starting a logic.

#  Simulation crawl_facebook  #
run the scripts as given in sequence below-

pagedetails.csv for each speaker
id,username,likes,talking_about_count,checkins
postdetails.csv
post id, share count, created time
username.csv
timestamp of post created , share count
listfbusers.csv- append mode, tracks all crawls
id,username,likes,talking_about_count,checkins
<username>.jpeg
final graphs

(ts vs shareCount)
put the new api key for getting the posts
read page ids got from facebook (fbPageIds.csv)
latest 100 posts are generated for every user as in fbPageIds.csv
see command_logs in logs folder
folders- crawl_facebook/fb_reports, crawl_facebook/fb_users gets generated
shukla@shukla-ubuntu:~/crawl_facebook$ python fbShareCount.py
draw plots for each user and number of shares
folder crawl_facebook/plot_graph_share gets generated
shukla@shukla-ubuntu:~/crawl_facebook$ python draw_plots_fb.py

(ts vs fblike)
for crawling like counts for a page everyday, folder with that day timestamp gets generated
folders- crawl_facebook/likeCount/fb_reports, crawl_facebook/likeCount/fb_users
shukla@shukla-ubuntu:~/crawl_facebook$ python fbLikeCount.py
generating graph data and plotting them
folder- crawl_facebook/likeCount/plot_graph_merge
draw plots of the merge results
folder- crawl_facebook/likeCount/plot_graph_merge
shukla@shukla-ubuntu:~/crawl_facebook$ python fbMergeDrawPlots.py
for merging each day crawl results and displaying them for all days timestamps
folders- crawl_facebook/likeCount/mergeLike, crawl_facebook/likeCount/FinalLikeResults
if mergeLike already exsists it is skipped and finalLikeResults is generated, otherwise both are generated
shukla@shukla-ubuntu:~/crawl_facebook$ python fbMergeResults.py

final results plotgraphs from here
 (ts vs fbShare vs google_trends)
1 col- googleTrends , 2 col- fbshare
folders- crawl_facebook$/graph_data_fbShare_Gt, crawl_facebook$/plot_graph_fbShare_Gt
shukla@shukla-ubuntu:~$ python graph_data_fbShare_Gt_wrapper.py db_keys
shukla@shukla-ubuntu:~/crawl_facebook$ python ../plot_graph_fbShareGt_wrapper.py ../db_keys

(ts vs fbShare vs retweets)
1 col retweets, 2col- fb_shares
folders- crawl_facebook$/graph_data_fbShare_Retweets, crawl_facebook$/plot_graph_fbShare_Retweets
shukla@shukla-ubuntu:~$ python graph_data_fbShare_Retweets_wrapper.py db_keys
shukla@shukla-ubuntu:~/crawl_facebook$ python ../plot_graph_fbShareRetweets_wrapper.py ../db_keys

(ts vs news vs fbShare)
see simulation intraceder

(ts vs twfollowers vs fbLikes)
folder- graph_data_fbLike_twfollowers
shukla@shukla-ubuntu:~$ python graph_data_fbLike_twfollowers_wrapper.py db_keys
folder-plot_graph_fbLike_twfollowers
shukla@shukla-ubuntu:~/crawl_facebook$ python ../plot_graph_fbLike_twfollowers_wrapper.py  ../db_keys

(ts vs twfollowers vs fbsharecount)
folder- graph_data_fbShare_twfollowers
shukla@shukla-ubuntu:~$ python graph_data_fbShare_twfollowers_wrapper.py db_keys
folder- plot_graph_fbShare_twfollowers
shukla@shukla-ubuntu:~/crawl_facebook$ python ../plot_graph_fbShare_twfollowers_wrapper.py  ../db_keys

(ts vs gt vs fbsharecount)
folder-graph_data_fbLike_gt
shukla@shukla-ubuntu:~$ python graph_data_fbLike_gt_wrapper.py db_keys
shukla@shukla-ubuntu:~/crawl_facebook$ python ../plot_graph_fbLike_gt_wrapper.py ../db_keys

(ts vs retweets vs fbsharecount)
folder-graph_data_fbLike_retweets
shukla@shukla-ubuntu:~$ python graph_data_fbLike_retweets_wrapper.py db_keys
shukla@shukla-ubuntu:~/crawl_facebook$ python ../plot_graph_fbLike_retweets_wrapper.py ../db_keys

correct crawled reports
folder-twitter_project_eurecom/utils
shukla@shukla-ubuntu:~/utils$ python remove_last_line.py /home/shukla/crawl_facebook/crawled_reports

# Simulation crawl_fanpagelist # 
shukla@shukla-ubuntu:~/test/like$ casperjs test2.js --userId=AdrianPeterson
CasperError: Cannot get information from div.today_stats_positive: no elements found.
  /usr/local/src/casperjs/modules/casper.js:1073 in getElementsInfo
  /home/shukla/test/like/test2.js:27
  /usr/local/src/casperjs/modules/casper.js:1557 in runStep
  /usr/local/src/casperjs/modules/casper.js:399 in checkStep
shukla@shukla-ubuntu:~/test/like$ casperjs test2.js --userId=AdrianPeterson
+ 91 Today
+ 1,020 Yesterday
+ 6 Today
+ 155 Yesterday

----------------------

works well for top celebrites who have pages on the site, have to check for spekers who have page, not for all, also needs to be authenticated

before was calling like this
shukla@shukla-ubuntu:~/Desktop/start/Link to tools/fbApi/Link to like$ casperjs test2.js --userId=AdrianPeterson


reads speakers from speakers.csv
calls api by casperjs(test2.js), passes arguments and genertes fb and twitter csv in seperate folders
moves them in folder for first time
for second time gets them out of folder, appends and puts them back
csv obtained has been tailored acc to need, every line with date a record
shukla@shukla-ubuntu:~/Desktop/start/Link to tools/fbApi/Link to like$ python simu.py 



--------------------
changes for crawling everyday, makes folder for timestamp
call 2-3 times
FbT and TwitterT gather logs of all crawlings and see status of all users, which are present on the site
shukla@shukla-ubuntu:~/crawl_fanpagelist$ python simu2.py

--------------------

#  Simulation crawl_newsCred  #
run the scripts as given in sequence below-

Different view count blog and mainstream and total
folder analysis_blog
shukla@shukla-ubuntu:~/crawl_newsCred/monthOut$  python getNum2Blog.py
folder analysisMainStream
shukla@shukla-ubuntu:~/crawl_newsCred/monthOut$  python getNum2MainStream.py
analysisTotal
gives the folder analysis, gives number of articled form date range specified, it reads the inputs from file 'speakers3.csv' and generates analysis folder for each user, page_size is fixed to 10
shukla@shukla-ubuntu:~/crawl_newsCred/monthOut$  python getNum2.py

crawl all news and in folder articles
gives the crawling by generating folder for each user and a csv with date range specified, it reads input from the file 'speakers3.csv' 
the response headers integrated and quoted strings helps to get rid %20
 give appropriatte page_size by looking at analysis for each user , its given normally 999, see for possible rate limits,
 if no of views is less than 2000 can give date interval of 15 days, greated than 9000 is better 1 per day in case of obama.
shukla@shukla-ubuntu:~/crawl_newsCred/monthOut$ python newsCred12.py

combine all graph data with news results
after getting all the graph data from all the operations, put them in folder and merge them
folders- see with 'merge; in allGraphData/fb,allGraphData/interceder,allGraphData/merge_crawled_data
7May- addred for ts vs gt vs interceder
9 may- added fpr twfollowers-fbshares-fblikes-interceder pairs
shukla@shukla-ubuntu:~$ python graph_data_merge_newsCred.py db_keys

merge fb results
9 may- added for twfollowers-fbshares-fblikes-interceder pairs
shukla@shukla-ubuntu:~$ python graph_data_merge_fbPosts.py db_keys

--------------------------------------
final results plot_graphs from here
--------------------------------------
calculate corelation
after getting all the graph data from all the operations, put them in folder and calculate corelation
shukla@shukla-ubuntu:~$ python graph_data_corelation.py db_keys

merge interceder corelation
after calculating corelation , run this script
folder-corelation_merge_interceder_all
retweets vs twfollowers vs fbshare vs fblikes vs gt
shukla@shukla-ubuntu:~$ python merge_data_interceder_corel_wrapper.py db_keys

after calculating corelation , run this script
folder-corelation_merge_fbShare_all
twfollowers vs gt vs retweets
shukla@shukla-ubuntu:~$ python merge_data_fbShare_corel_wrapper.py db_keys

after calculating corelation , run this script
folder-corelation_merge_fbLikes_all
twfollowers vs retweets vs fblikes
shukla@shukla-ubuntu:~$ python merge_data_fbLike_corel_wrapper.py db_keys