#!/usr/bin/env python

#import db_handling as hand
import os,sys,inspect,time
from subprocess import call
import csv

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
sys.path.insert(0,parentdir)
#import db_handling as han




class DrawGraphsFb:

    '''
    def __init__(self,db_keys):

        self.handler = han.DBHandler(db_keys)
    """
    It takes all the files in the 'reports' folder and calls simple_octave script for each csv.
    """
    def plot_fb(self):                #make plots according to type given
    '''
    #print len(os.listdir('../reports'))
    #speakers = self.handler.get_users_with_fullname_username()

    testDic={}
    listFbUsers = parentdir+'/crawl_facebook/fbPageIds.csv'
    #reading crawled results
    with open('%s'%(listFbUsers), mode='r') as infile:
        reader = csv.reader(infile)
        for row in reader:
            temp= row[0]
            var2= temp.split(';')
            #print var2[0]
            #print var2
            #testDic[var2[0]]=var2[4]
            #testDic[var2[0]]=var2[1]
            testDic.setdefault(var2[0],[]).append(str(var2[1]))
            testDic.setdefault(var2[0],[]).append(str(var2[4]))
            testDic.setdefault(var2[0],[]).append(str(var2[5]))

        print testDic


    for key in testDic:
        folder_name4 = parentdir+'/crawl_facebook/likeCount/plot_graph_merge/'
        if not os.path.exists(folder_name4):
            os.makedirs(folder_name4)
        folder_name3 = parentdir+'/crawl_facebook/likeCount/FinalLikeResults/%s'%str(testDic[key][0])

        try:                                 #run octave script on the contents of csv taking 'x' as google trend volume and 'y' as timestamp

            print "SIMPLEE"
            print '%s/%s.csv'%(folder_name3,str(testDic[key][0]))

            #print os.listdir('./crawl_facebook')
            #print parentdir
            call(["octave","--silent", "--eval","simple_octave_merge", "%s/%s.csv"%(folder_name3,str(testDic[key][0])),"%s/%s"%(folder_name4,str(testDic[key][0]))+".jpeg","%s-%s-%s"%(str(testDic[key][0]),str(testDic[key][1]),str(testDic[key][2]))]) #calling bash commands for octave



        except Exception as e:
            print e






